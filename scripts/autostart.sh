#!/usr/bin/bash

# ===============================================================================
# This script is run by the WM on startup. Use it to start any programs on login.
# ===============================================================================

# Set monitor configuration, bluelight filter, and wallpaper
autorandr -c && redshift -P -O 3600 && nitrogen --restore &

# Enable transparency
picom -b &

# Start polybar
~/.config/polybar/launch.sh --material &

# Enable desktop notifications
dunst &

# Start programs
mattermost-desktop &
android-studio &
