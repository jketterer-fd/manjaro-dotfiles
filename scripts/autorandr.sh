#!/usr/bin/bash

# This will change the monitor configuration based on the connected monitor
# TODO: change xrandr command based on work monitors
xrandr | grep "HDMI2 connected\|HDMI-2 connected"
if [[ $? == 0 ]]; then
	autorandr --change home
else
	autorandr --change mobile
fi

# Note: This script is run automatically on udev monitor change hook, in /etc/udev/rules.d/95-monitors.rules
